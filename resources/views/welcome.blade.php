<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sceon.am</title>
    <style>
        * {
            padding: 0;
            margin: 0;
        }
        .sceon-body {
            display: flex;
            width: 100%;
            height: 100vh;
            background-color: #15121E;
            justify-content: center;
            align-items: center;
            background-image: url("{{url('/')}}/images/api-bg.png");
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
        }
        .sceon-body img {
            width: auto;
            height: 100px;
            display: inline-block;
        }
    </style>
</head>
<body>

<div class="sceon-body">
    <img src="{{url('/')}}/images/sceon-logo.png" alt="">
</div>
</body>
</html>
