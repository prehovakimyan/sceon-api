<?php

use App\Http\Controllers\Api\Cellar\BlogController;
use App\Http\Controllers\Api\TagsController;
use App\Http\Controllers\Api\Cellar\UserManagementController;
use App\Http\Controllers\Api\Cellar\VacanciesController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);


Route::middleware('auth:sanctum')->group(function () {
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::get('/me', [AuthController::class, 'me']);
});

//Routes Access for User Roles / administrator, moderator, blog manager /
Route::group(['middleware' => ['auth:sanctum', 'role:administrator|moderator|blog-manager']], function() {
    Route::apiResources([
        'cellar/blog' => BlogController::class,
    ]);
});

//Routes Access for User Roles / administrator /
Route::group(['middleware' => ['auth:sanctum', 'role:administrator|moderator']], function() {
    Route::apiResources([
        'cellar/vacancies' => VacanciesController::class,
        'cellar/tags' => TagsController::class,
    ]);
});

//Routes Access for User Roles / moderator /
Route::group(['middleware' => ['auth:sanctum', 'role:administrator']], function() {
    Route::apiResources([
        'cellar/user-management' => UserManagementController::class,

    ]);
});


//Frontend Routes

Route::get('/login', [AuthController::class, 'login']);



