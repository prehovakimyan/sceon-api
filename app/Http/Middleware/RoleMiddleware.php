<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle($request, Closure $next, $role, $permission = null)
    {
        $roles = explode('|', $role);

        if(!$request->user()->hasRole($roles)) {

            return response()->json([
                'status' => false,
                'message' => 'This Role dosn`t have permission',
            ], 404);

        }

        if($permission !== null && !$request->user()->can($permission)) {

            return response()->json([
                'status' => false,
                'message' => 'This Role not have permission',
            ], 404);

        }

        return $next($request);

    }
}
