<?php

namespace App\Http\Controllers\Api\Cellar;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserManagementController extends Controller
{

    private $permissionsList = [
        'administrator' => ['user-management', 'vacancies', 'blog'],
        'moderator' => ['vacancies', 'blog'],
        'blog-manager' => ['blog'],
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        if($users){
            foreach ($users as $user) {
                $user->role_name = $user->roleName($user->userRoles->role_id);
            }
        }

        return response()->json([
            'status' => true,
            'response' => $users,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try{

            $adminUser = $request->user();

            $validateUser = Validator::make($request->all(),
                [
                    "name"     => "required|string|max:255",
                    "email"    => "required|email|string|max:255|unique:users,email",
                    'password' => 'required|string|confirmed|min:6',
                    'role'     => 'required',
                ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }


            $user = new User();
            $user->name = trim($request->name);
            $user->email = trim($request->email);
            $user->password = bcrypt(trim($request->password));
            $user->created_by = $adminUser->id;
            $user->updated_by = $adminUser->id;
            $user->save();

            $user_role = Role::where('slug', $request->role)->first();
            if($user_role) {
                $user->roles()->attach($user_role);

                $userPermissionsList = $this->permissionsList[$request->role];

                foreach ($userPermissionsList as $userPermission) {
                    $permission = Permission::where('name', $userPermission)->first();
                    $user->permissions()->attach($permission);
                }
            }

            return response()->json([
                'status' => true,
                'message' => $user->id
            ], 200);

        }catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }






    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $status = false;


        if($user) {
            $status = true;
            $user->role_name = $user->roleName($user->userRoles->role_id);
        }

        return response()->json([
            'status' => $status,
            'message' => $user,
        ], 200);



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if($user) {

            $adminUser = $request->user();


            $validateUser = Validator::make($request->all(),
                [
                    "name"     => "required|string|max:255",
                    "email"    => "required|email|unique:users,email,".$id,
                    'role'     => 'required',
                ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            $user->name = trim($request->name);
            $user->email = trim($request->email);
            $user->password = bcrypt(trim($request->password));
            $user->created_by = $adminUser->id;
            $user->updated_by = $adminUser->id;
            $user->save();

            return response()->json([
                'status' => true,
                'message' => $id
            ], 200);

        } else {
            return response()->json([
                'status' => false,
                'message' => 'error',
                'errors' => 'User not found'
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{

            User::destroy($id);

            return response()->json([
                'status' => true,
                'message' => 'User is deleted successfully'
            ], 200);
        }catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
}
