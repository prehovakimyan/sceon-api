<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Model::unguard();

        Schema::disableForeignKeyConstraints();

        DB::table('roles')->truncate();

        Schema::enableForeignKeyConstraints();


        $users_permission = Permission::where('name', 'user-management')->first();
        $vacancies_permission = Permission::where('name', 'vacancies')->first();
        $blog_permission = Permission::where('name', 'blog')->first();


        $admin_role = new Role();
        $admin_role->slug = 'administrator';
        $admin_role->name = 'admin';
        $admin_role->save();


        $moderator_role = new Role();
        $moderator_role->slug = 'moderator';
        $moderator_role->name = 'moderator';
        $moderator_role->save();

        $blog_role = new Role();
        $blog_role->slug = 'blog-manager';
        $blog_role->name = 'blog-manager';
        $blog_role->save();

        Model::reguard();



        //$dev_role->permissions()->attach($dev_permission);

    }
}
