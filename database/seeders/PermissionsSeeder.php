<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Schema::disableForeignKeyConstraints();

        DB::table('permissions')->truncate();
        DB::table('roles_permissions')->truncate();

        Schema::enableForeignKeyConstraints();

        $admin_role = Role::where('slug', 'administrator')->first();
        $moderator_role = Role::where('slug', 'moderator')->first();
        $blog_manager_role = Role::where('slug', 'blog-manager')->first();

        //userManagement
        $permissionUserManagement = new Permission();
        $permissionUserManagement->slug = 'User Resource';
        $permissionUserManagement->name = 'user-management';
        $permissionUserManagement->save();
        $permissionUserManagement->roles()->attach($admin_role);

        //Vacancies
        $permissionVacancies = new Permission();
        $permissionVacancies->slug = 'Vacancies';
        $permissionVacancies->name = 'vacancies';
        $permissionVacancies->save();
        $permissionVacancies->roles()->attach($admin_role);
        $permissionVacancies->roles()->attach($moderator_role);

        //Blog
        $permissionBlog = new Permission();
        $permissionBlog->slug = 'User Resource';
        $permissionBlog->name = 'blog';
        $permissionBlog->save();
        $permissionBlog->roles()->attach($admin_role);
        $permissionBlog->roles()->attach($moderator_role);
        $permissionBlog->roles()->attach($blog_manager_role);


        Model::reguard();
    }
}
