<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Schema::disableForeignKeyConstraints();

        DB::table('users')->truncate();
        DB::table('users_roles')->truncate();
        DB::table('users_permissions')->truncate();

        Schema::enableForeignKeyConstraints();

        $admin_role = Role::where('slug', 'administrator')->first();
        $moderator_role = Role::where('slug', 'moderator')->first();
        $blog_manager_role = Role::where('slug', 'blog-manager')->first();

        $users_permission = Permission::where('name', 'user-management')->first();
        $vacancies_permission = Permission::where('name', 'vacancies')->first();
        $blog_permission = Permission::where('name', 'blog')->first();

        $user = new User();
        $user->name = 'Paruyr Hovakimyan';
        $user->email = 'paruyrhovakimyan@gmail.com';
        $user->password = bcrypt('asdasdasd');
        $user->save();
        $user->roles()->attach($admin_role);
        $user->permissions()->attach($users_permission);
        $user->permissions()->attach($vacancies_permission);
        $user->permissions()->attach($blog_permission);

        $userAra = new User();
        $userAra->name = 'Paruyr Hovakimyan';
        $userAra->email = 'paruyrhovakimyan@gmail.com';
        $userAra->password = bcrypt('asdasdasd');
        $userAra->save();
        $userAra->roles()->attach($admin_role);
        $userAra->permissions()->attach($users_permission);
        $userAra->permissions()->attach($vacancies_permission);
        $userAra->permissions()->attach($blog_permission);



        Model::reguard();
    }
}
