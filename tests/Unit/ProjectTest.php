<?php

namespace Tests\Unit;

use Tests\TestCase;

class LoginTest extends TestCase
{

    public $endpoint = 'login';

    /**
     *  Login with empty params
     */
    public function testEmptyPostRequestForLogin()
    {
        $response = $this->post('/'.$this->api.'/'.$this->endpoint);
        $this->assertEquals(401, $response->status());
    }

    /**
     *  Login with valid params
     */
    public function testValidPostRequestForLogin()
    {
        $response = $this->post('/'.$this->api.'/'.$this->endpoint, [
            'email'=> 'mahedi@gmail.com',
            'password' => 'secrettt'
        ]);
        $response->assertOk();
        $this->assertEquals('User Logged In Successfully', json_decode($response->getContent(), true)['message']);
    }

    /**
     *  Login with invalid params
     */
    public function testInvalidPostRequestForLogin()
    {
        $response = $this->post('/'.$this->api.'/'.$this->endpoint, [
            'email'=> 'mahedi@gmail.com',
            'password' => 'invalid password'
        ]);
        $this->assertEquals(401, $response->status());
        $this->assertEquals('Email & Password does not match with our record.', json_decode($response->getContent(), true)['message']);
    }

    /**
     *   Login with invalid params
     */
    public function testInvalidPostRequestForLoginValidationError()
    {
        $response = $this->post('/'.$this->api.'/'.$this->endpoint, [
            'email'=> '',
            'password' => 'invalid password'
        ]);
        $this->assertEquals(401, $response->status());
        $this->assertEquals('validation error', json_decode($response->getContent(), true)['message']);
    }
}
