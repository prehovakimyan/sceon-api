<?php

namespace Tests\Unit;

use Tests\TestCase;

class RegistrationTest extends TestCase
{
    public $endpoint = 'register';
    /**
     *  Registration with invalid email
     */
    public function testRegistrationWithInvalidEmail()
    {
        $response = $this->post('/'.$this->api.'/'.$this->endpoint, [
            'name' => 'Name',
            'email' => 'testdf',
            'password' => '123548465431',
        ]);

        $this-> assertEquals(400, $response->status());
    }

    /**
     *  Registration with invalid password
     */
    public function testRegistrationWithInvalidPassword()
    {
        $response = $this->post('/'.$this->api.'/'.$this->endpoint, [
            'name' => 'Name',
            'email' => 'validEmail@gmail.com',
            'password' => '12',
        ]);

        $this-> assertEquals(400, $response->status());
    }

    /**
     *  Registration with invalid name
     */
    public function testRegistrationWithInvalidName()
    {
        $response = $this->post('/'.$this->api.'/'.$this->endpoint, [
            'name' => '',
            'email' => 'validEmail@gmail.com',
            'password' => '125465131334651',
        ]);

        $this-> assertEquals(400, $response->status());
    }


    /**
     *  Registration with valid data
     */
    public function testRegistrationWithValidData()
    {
        $response = $this->post('/'.$this->api.'/'.$this->endpoint, [
            'name' => 'testName',
            'email' => 'validEmail@gmail.com',
            'password' => '125465131334651',
        ]);

        $this-> assertEquals(400, $response->status());
    }
}
